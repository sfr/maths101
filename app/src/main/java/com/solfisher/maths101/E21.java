package com.solfisher.maths101;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class E21 extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView lv;

    private ArrayList<String> choices;
    private ArrayAdapter<String> adapter;
    private SharedPreferences share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e21);

        lv = findViewById(R.id.lv21);

        // create list of options
        choices = new ArrayList<String>();
        choices.add("-7");
        choices.add("7");
        choices.add("-17");
        choices.add("17");

        // connect options to listview
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, choices);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home,menu);
        return true;
    }

    // return to home screen
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
        share = getSharedPreferences(getString(R.string.sharedprefs_dir), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = share.edit();
        editor.putBoolean("e21pass", (i == 2)); // third option is correct
        editor.commit();

        // return to home screen
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
