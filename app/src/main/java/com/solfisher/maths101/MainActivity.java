package com.solfisher.maths101;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button b11, b12, b13, b21, b22, b23, b3;
    private SharedPreferences share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // find buttons
        b11 = findViewById(R.id.b11);
        b12 = findViewById(R.id.b12);
        b13 = findViewById(R.id.b13);
        b21 = findViewById(R.id.b21);
        b22 = findViewById(R.id.b22);
        b23 = findViewById(R.id.b23);
        b3 = findViewById(R.id.b3);

        // load sharedprefs
        share = getSharedPreferences(getString(R.string.sharedprefs_dir), Context.MODE_PRIVATE);

        // find if answer was correct, and change the color if so
        if (share.getBoolean("e11pass", false))
            b11.setBackgroundColor(Color.parseColor("#00aa00"));
        if (share.getBoolean("e12pass", false))
            b12.setBackgroundColor(Color.parseColor("#00aa00"));
        if (share.getBoolean("e13pass", false))
            b13.setBackgroundColor(Color.parseColor("#00aa00"));
        if (share.getBoolean("e21pass", false))
            b21.setBackgroundColor(Color.parseColor("#00aa00"));
        if (share.getBoolean("e22pass", false))
            b22.setBackgroundColor(Color.parseColor("#00aa00"));
        if (share.getBoolean("e23pass", false))
            b23.setBackgroundColor(Color.parseColor("#00aa00"));
        if (share.getBoolean("e3pass", false))
            b3.setBackgroundColor(Color.parseColor("#00aa00"));
    }

    /*
    * onClick actions
    * each button sends you to a different activity.
    */

    public void e11(View view) {
        Intent intent = new Intent(this, E11.class);
        startActivity(intent);
        finish();
    }

    public void e12(View view) {
        Intent intent = new Intent(this, E12.class);
        startActivity(intent);
        finish();
    }

    public void e13(View view) {
        Intent intent = new Intent(this, E13.class);
        startActivity(intent);
        finish();
    }

    public void e21(View view) {
        Intent intent = new Intent(this, E21.class);
        startActivity(intent);
        finish();
    }

    public void e22(View view) {
        Intent intent = new Intent(this, E22.class);
        startActivity(intent);
        finish();
    }

    public void e23(View view) {
        Intent intent = new Intent(this, E23.class);
        startActivity(intent);
        finish();
    }

    public void e3(View view) {
        Intent intent = new Intent(this, E3.class);
        startActivity(intent);
        finish();
    }
}